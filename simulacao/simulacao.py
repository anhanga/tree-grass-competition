import numpy as np
from statistics import mean
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from automata import *
import sympy as sm
import math

t = sm.symbols('t')
freqChuva = sm.Function('lambda')(t)
stress = sm.Function('sigma')(freqChuva)
stress = (math.e)**(-freqChuva/0.2)

def qtdPorEstresse(n, freqsChuvas, numIteracoes, condInicial, numExec):
    qtdFinal = {'A': [], 'M': [], 'V': [], 'G': []}
    for a in freqsChuvas:
        print('Calculando p/ freqChuva =', a)
        freqChuva = a + 0*t
        stress = (math.e)**(-freqChuva/0.2)
        tg = automata(n, stress, t, numIteracoes, condInicial = condInicial)
        tg.nExec(numExec)
        qtd = tg.qtdMedia
        print('Vazio final:', mean(qtd['V'][numIteracoes-15:numIteracoes]))
        for k in qtdFinal:
            qtdFinal[k].append(mean(qtd[k][numIteracoes-15:numIteracoes]))
            #tempo = tg.tempo
    
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_title('Populações finais após '+ str(numIteracoes) +
                 ' iterações em função da frequência de chuvas')
    ax.set_xlabel('Freq. chuvas (1/dia)')
    ax.set_ylabel('População final (%)')
    ax.plot(freqsChuvas, qtdFinal['A'], label='Arvores', color='green')
    ax.plot(freqsChuvas, qtdFinal['V'], label='Vazio', color='gray')
    ax.plot(freqsChuvas, qtdFinal['M'], label='Muda', color='blue')
    ax.plot(freqsChuvas, qtdFinal['G'], label='Grama', color='#CC881A')
    ax.legend()

    plt.show()

def qtdPorTempo(n, numIteracoes, condInicial, numExec):
    stress = (math.e)**(-freqChuva/0.2)
    tg = automata(n, stress, t, numIteracoes, condInicial = condInicial)
    tg.nExec(numExec)
    qtd = tg.qtdMedia
    tempo = tg.tempo
    matriz = np.array(tg.gridToInt())

    chuvaVals = []
    for i in tempo:
        chuvaVals.append(freqChuva.evalf(subs={t: i}))

    fig, ax = plt.subplots(3)
    fig2, ax2 = plt.subplots()

    ax[0].grid(True)
    ax[0].set_xlabel('Tempo (ano)')
    ax[0].set_ylabel('Quantidade (%)')
    ax[0].plot(tempo, qtd['A'], label='Arvores', color='green')
    ax[0].plot(tempo, qtd['V'], label='Vazio', color='gray')
    ax[0].plot(tempo, qtd['M'], label='Muda', color='blue')
    ax[0].plot(tempo, qtd['G'], label='Grama', color='#CC881A')
    ax[0].legend()

    ax[1].grid(True)
    ax[1].set_xlabel('Tempo (ano)')
    ax[1].set_ylabel('Freq. Chuvas (1/dia)')
    ax[1].plot(tempo, chuvaVals, label="Frequência de chuvas", color='red')
    ax[1].legend()

    ax[2].grid(True)
    ax[2].set_xlabel('Qtd. Arvores (%)')
    ax[2].set_ylabel('Qtd. Gramas (%)')
    ax[2].plot(qtd['A'], qtd['G'], color='purple')
    ax[2].legend()

    ax2.matshow(matriz, cmap = colors.ListedColormap(['#EBDBB2', '#B8BB26', '#83A598', '#CC881A']))
    ax2.axis(False)
    plt.show()

# Parâmetros para a execução das simulações, alterados de acordo com a 
# simulação a ser realizada
############################################################################

#n = 80
#condInicial = {'V': 25, 'M': 25, 'G': 25, 'A': 25}
#numIteracoes = 200
#numExec = 1

#freqChuva = 1 - t/200
#qtdPorTempo(n, numIteracoes, condInicial, numExec)

#freqsChuvas = np.arange(0.1, 0.5, 0.005)
#qtdPorEstresse(n, freqsChuvas, numIteracoes, condInicial, numExec)
